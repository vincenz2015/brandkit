
/* Define resources */
var resourceItems = {
    'images': {
        'name': 'images',
        'icon': 'images/icon-cam.png',
        'desc': 'Convey New Zealand<br>and your business through<br>professional visuals.'
    },
    'info': {
        'name': 'infographics<br>& statistics',
        'icon': 'images/icon-graph.png',
        'desc': 'Use facts and figures to<br>prove that New Zealand is<br>great at doing business.'
    },
    'presentation': {
        'name': 'Presentation templates',
        'icon': 'images/icon-doc.png',
        'desc': 'Templates to create your<br>own marketing tools.'
    },
    'videos': {
        'name': 'Videos',
        'icon': 'images/icon-video.png',
        'desc': 'Set the scene with the<br>NZ Story video available in<br>multiple languages.'
    },
    'advice': {
        'name': 'Advice on regions',
        'icon': 'images/icon-advice.png',
        'desc': 'Review the best practices<br>for doing trade in different<br>countries and sectors.'
    },
    'cases': {
        'name': 'Case studies',
        'icon': 'images/icon-people.png',
        'desc': 'Learn from real<br>New Zealand business<br>owners’ experiences.'
    }
};

/* Render resources */
function renderResources() {
    var resourceHtml = '';
    for (var key in resourceItems) {
        var item = resourceItems[key];
        resourceHtml += '<div class="col-12 col-md-4">';
        resourceHtml += '<div class="box-white">';
        resourceHtml += '<div class="arrow"><img src="images/arrow.png"></div>';
        resourceHtml += '<div class="img-cont"><img src="' + item.icon + '"></div>';
        resourceHtml += '<span class="text">' + item.name + '</span>';
        resourceHtml += '<div class="desc clearfix">';
        resourceHtml += item.desc;
        resourceHtml += '</div></div></div>';
    }
    $('#resource_items').html(resourceHtml);
}

/* Show resources */
$(function () {
    renderResources();
});